#!/usr/bin/env python3
# coding: utf-8

"""
SY31 _ PROJET COLIN LAFOND
"""

import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt

############################################ PARTIE MODELISATION DE LA POSITION DU ROBOT

def modelisation_position_du_robot(df_encoder: pd.DataFrame) -> pd.DataFrame:

    r = 0.033
    delta_t = 0.05
    L = 0.08

    df_encoder['field.header.stamp'] = (df_encoder['field.header.stamp']
                                        - df_encoder['field.header.stamp'].tolist()[0])/1e9

    df_encoder.set_index('field.header.stamp',inplace=True)

    df_encoder['v'] = df_encoder['field.left_encoder'].astype(float)
    df_encoder['v'].values[0] = 0
    df_encoder['x'] = df_encoder['v']
    df_encoder['x'].values[0] = 0
    df_encoder['y'] = df_encoder['v']
    df_encoder['y'].values[0] = 0
    df_encoder['omega'] = df_encoder['v']  # lacet
    df_encoder['omega'].values[0] = 0
    df_encoder['theta'] = df_encoder['v']  # lacet
    df_encoder['theta'].values[0] = 0

    for i in range(1, len(df_encoder)):

        # Variation d'angle des roues
        angle_delta_r = (df_encoder['field.right_encoder'].values[i] - df_encoder['field.right_encoder'].values[i-1])*np.pi/2048.0
        angle_delta_l = (df_encoder['field.left_encoder'].values[i] - df_encoder['field.left_encoder'].values[i-1])*np.pi/2048.0

         # vitesse  du robot
        df_encoder['v'].values[i] = (r*(angle_delta_r + angle_delta_l)/2.0)/delta_t
        df_encoder['omega'].values[i] = (r*(angle_delta_r - angle_delta_l)/(2.0*L))/delta_t

        # Angle theta du robot
        df_encoder['theta'].values[i] = df_encoder['theta'].values[i-1] + delta_omega
        # positions x du robot
        df_encoder['x'].values[i] = (df_encoder['x'].values[i-1] + delta_v*np.cos(df_encoder['theta'].values[i]))
        # positions x du robot
        df_encoder['y'].values[i] = (df_encoder['y'].values[i-1] + delta_v*np.sin(df_encoder['theta'].values[i]))

    return df_encoder

# Fonction pour modéliser l'environement du robo
def modelisation_environement(donneesDuLidar pd.DataFrame, position_du_robot: pd.DataFrame) -> np.ndarray:


    # change index
    donneesDuLidar['field.header.stamp'] = (['field.header.stamp'] - donneesDuLidar['field.header.stamp'].tolist()[0]) / 1e9

    donneesDuLidar.set_index('field.header.stamp', inplace=True)

    environement = []

    temps_i = 0

    for _time, row in donneesDuLidar.iterrows():
        n = 360  # <-- number of points
        rang = np.array(row[10:10+n])  # pour les angles
        intensites = np.array(row[10+n:10+2*n])  # pour les intensitées (couleurs)

        # Tableau de tous les angles en rad.
        theta = row['field.angle_min'] + np.array(range(n))*row['field.angle_increment']

        # Compensation des angles de manière à ce que l'environnement cartographié par le
        # LiDAR ne "tourne" pas si le robot tourne.
        theta = theta + position_du_robot['theta'].values[temps_i]

            scan = np.array([rang*np.cos(theta),
                         rang*np.sin(theta),
                         intensites])  # [np.array : x, np.array: y]

        scan = scan[:, (rang > row['field.range_min']) & (rang < row['field.range_max'])]

        environement.append(scan)

        temps_i += 1  # index utilisé pour la stabilisation

    return environement



# Fonction gérant l'affichage du robot dans son environnement

def affichage_robot_environnement(donneesDuLidar: np.ndarray,  position_du_robot: pd.DataFrame) -> None:

    # Changement du repère : le robot avance dans le repère du lidar
    for i in range(len(donneesDuLidar)):
        for j in range(len(donneesDuLidar[i][0])):
            donneesDuLidar[i][0][j] += position_du_robot['x'].iloc[i]
            donneesDuLidar[i][1][j] += position_du_robot['y'].iloc[i]

    # Représentation à l'aide de matplotlib sous forme de points
    for i in range(len(donneesDuLidar)):
        plt.scatter(donneesDuLidar[i][0],
                    donneesDuLidar[i][1],
                    c=donneesDuLidar[i][2],
                    label='Environnement')
        plt.scatter(position_du_robot['x'].iloc[i],
                    position_du_robot['y'].iloc[i],
                    c='red',
                    label='Robot')

        plt.title('Visualisation de l''environement du robot ainsi que son déplacement au cours du temps')
        plt.axis('equal')  # pour avoir un repère orthonormé
        plt.xlabel('Axe X en m')
        plt.ylabel('Axe Y en m')
        plt.legend()
        plt.draw()
        plt.pause(0.001)
        plt.clf()


if __name__ == '__main__':

    SCENARIO = 2  # Changer cette valeur selon le scénario voulu (1, 2, 3 ou 4)

    DF_LIDAR = pd.read_csv("scenario_%d/scan.csv" % SCENARIO)
    DF_ENCODER = pd.read_csv("scenario_%d/sensor_state.csv" % SCENARIO)

    AVERAGE_GROUP_NUMBER = len(DF_ENCODER)/len(DF_LIDAR)

# réctification du temps pour les deux capteurs

    for i in range(1, len(DF_LIDAR)):
        for j in range(math.ceil(i*AVERAGE_GROUP_NUMBER)-1,
                       math.ceil((i-1)*AVERAGE_GROUP_NUMBER),
                       -1):

            DF_ENCODER = DF_ENCODER.drop(j)

    DF_ENCODER.reset_index(drop=True, inplace=True)  # réajustement des index à partir de 0.

    POSITION_DU_ROBOT = modelisation_position_du_robot(DF_ENCODER)

    DONNEES_LIDAR = modelisation_environement(DF_LIDAR, POSITION_DU_ROBOT)

    # Affichage de la position du robot
    affichage_robot_environnement(DONNEES_LIDAR, POSITION_DU_ROBOT)
